package VetClinic.Menu;

import VetClinic.Dao.ConsultDao;
import VetClinic.Dao.PetDao;
import VetClinic.Dao.VeterinarianDao;
import VetClinic.Model.Consult;
import VetClinic.Model.Pet;
import VetClinic.Model.Veterinarian;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private Scanner in = new Scanner(System.in);
    private PetDao petDao = new PetDao();
    private VeterinarianDao veterinarianDao = new VeterinarianDao();
    private ConsultDao consultDao = new ConsultDao();

    public void displayMenu(){
        System.out.println("Choose opton:\n" +
                "1.  Show vets\n" +
                "2.  Create vet\n" +
                "3.  Update vet\n" +
                "4.  Delete vet\n" +
                "5.  Show pets\n" +
                "6.  Create pet\n" +
                "7.  Update pet\n" +
                "8.  Delete pet\n" +
                "9.  Show consultations\n" +
                "10. Create consultation\n" +
                "11. Update consultation\n" +
                "12. Delete consultation\n\n" +
                "0.  Exit");
    }

    public int chooseOption(){
        int option = in.nextInt();
        in.nextLine();
        switch (option){
            case 0:
                break;
            case 1:
                showVets();
                break;
            case 2:
                createVet();
                break;
            case 3:
                updateVet();
                break;
            case 4:
                deleteVet();
                break;
            case 5:
                showPets();
                break;
            case 6:
                createPet();
                break;
            case 7:
                updatePet();
                break;
            case 8:
                deletePet();
                break;
            case 9:
                showConsults();
                break;
            case 10:
                createConsult();
                break;
            case 11:
                updateConsult();
                break;
            case 12:
                deleteConsult();
                break;
            default:
                System.out.println("ILLEGAL OPTION");
                this.chooseOption();
        }

        return 1;
    }

    private void showVets(){
        List<Veterinarian> vetList = veterinarianDao.getAllVets();
        System.out.println(vetList);
    }

    private void showPets(){
        List<Pet> petList = petDao.getAllPets();
        System.out.println(petList);
    }

    private void showConsults(){
        List<Consult> consultList = consultDao.getAllConsults();
        System.out.println(consultList);
    }

    private void createVet(){
        System.out.println("First Name: ");
        String firstName = in.nextLine();

        System.out.println("Last Name: ");
        String lastName = in.nextLine();

        System.out.println("Address: ");
        String address = in.nextLine();

        System.out.println("Specialty: ");
        String specialty = in.nextLine();

        Veterinarian vet = new Veterinarian(firstName, lastName, address, specialty);
        veterinarianDao.createVet(vet);
    }

    private void createPet(){
        System.out.println("Species: ");
        String species = in.nextLine();

        System.out.println("Birth date:\n" +
                "year: ");
        int birthYear = in.nextInt();
        System.out.println("month: ");
        int birthMonth = in.nextInt();
        System.out.println("day: ");
        int birthDay = in.nextInt();
        GregorianCalendar birthDate = new GregorianCalendar(birthYear,birthMonth-1,birthDay);

        System.out.println("Vaccinated: ");
        boolean vaxxed = in.nextBoolean();

        in.nextLine();
        System.out.println("Owner name: ");
        String ownerName = in.nextLine();

        Pet pet = new Pet(species, birthDate, vaxxed, ownerName);
        petDao.createPet(pet);
    }

    private void createConsult(){
        System.out.println("Create a new Consultation");
        System.out.println("Select veterinarian from list: ");
        showVets();
        int vetSelect = in.nextInt();
        Veterinarian vet = veterinarianDao.findVetById(vetSelect);

        System.out.println("Select pet from list:");
        showPets();
        int petSelect = in.nextInt();
        Pet pet = petDao.findPetById(petSelect);

        System.out.println("Enter date:\n" +
                "year: ");
        int consultYear = in.nextInt();
        System.out.println("month: ");
        int consultMonth = in.nextInt();
        System.out.println("day: ");
        int consultDay = in.nextInt();
        GregorianCalendar consultDate = new GregorianCalendar(consultYear, consultMonth-1, consultDay);

        in.nextLine();
        System.out.println("Description: ");
        String description = in.nextLine();

        Consult consult = new Consult(vet, pet, consultDate, description);
        consultDao.createConsult(consult);
    }

    private void updateVet(){
        showVets();
        System.out.println("Select vet:");
        int vet = in.nextInt();
        in.nextLine();
        Veterinarian vetDel = veterinarianDao.findVetById(vet);

        System.out.println("First Name: ");
        vetDel.setFirstName(in.nextLine());

        System.out.println("Last Name: ");
        vetDel.setLastName(in.nextLine());

        System.out.println("Address: ");
        vetDel.setAddress(in.nextLine());

        System.out.println("Specialty: ");
        vetDel.setSpecialty(in.nextLine());

        veterinarianDao.updateVet(vetDel);
    }

    private void updatePet(){
        showPets();
        System.out.println("Select pet:");
        int pet = in.nextInt();
        in.nextLine();
        Pet petDel = petDao.findPetById(pet);

        System.out.println("Species: ");
        String species = in.nextLine();

        System.out.println("Birth date:\n" +
                "year: ");
        int birthYear = in.nextInt();
        System.out.println("month: ");
        int birthMonth = in.nextInt();
        System.out.println("day: ");
        int birthDay = in.nextInt();
        GregorianCalendar birthDate = new GregorianCalendar(birthYear,birthMonth-1,birthDay);

        System.out.println("Vaccinated: ");
        boolean vaxxed = in.nextBoolean();

        in.nextLine();
        System.out.println("Owner name: ");
        String ownerName = in.nextLine();

        petDao.updatePet(petDel);
    }

    private void updateConsult(){
        showConsults();
        System.out.println("Select consult: ");
        int consult = in.nextInt();
        in.nextLine();
        Consult consultDel = consultDao.findConsultById(consult);

        System.out.println("Create a new Consultation");
        System.out.println("Select veterinarian from list: ");
        showVets();
        int vetSelect = in.nextInt();
        Veterinarian vet = veterinarianDao.findVetById(vetSelect);

        System.out.println("Select pet from list:");
        showPets();
        int petSelect = in.nextInt();
        Pet pet = petDao.findPetById(petSelect);

        System.out.println("Enter date:\n" +
                "year: ");
        int consultYear = in.nextInt();
        System.out.println("month: ");
        int consultMonth = in.nextInt();
        System.out.println("day: ");
        int consultDay = in.nextInt();
        GregorianCalendar consultDate = new GregorianCalendar(consultYear, consultMonth-1, consultDay);

        in.nextLine();
        System.out.println("Description: ");
        String description = in.nextLine();

        consultDao.createConsult(consultDel);
    }

    private void deleteVet(){
        showVets();
        System.out.println("Select vet: ");
        int vet = in.nextInt();
        Veterinarian vetDel = veterinarianDao.findVetById(vet);
        veterinarianDao.deleteVet(vetDel);
    }

    private void deletePet(){
        showPets();
        System.out.println("Select pet: ");
        int pet = in.nextInt();
        Pet petDel = petDao.findPetById(pet);
        petDao.deletePet(petDel);
    }

    private void deleteConsult(){
        showConsults();
        System.out.println("Select consultation: ");
        int consult = in.nextInt();
        Consult consultDel = consultDao.findConsultById(consult);
        consultDao.deleteConsult(consultDel);
    }
}
