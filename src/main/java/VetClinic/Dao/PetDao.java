package VetClinic.Dao;

import VetClinic.HibernateUtils;
import VetClinic.Model.Pet;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PetDao {
    public List<Pet> getAllPets (){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Pet> pet = session.createQuery("from Pet", Pet.class).list();
            return pet;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void createPet(Pet pet) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(pet);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Pet findPetById(long id){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Pet pet = session.find(Pet.class, id);
            return pet;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void updatePet(Pet pet){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(pet);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void deletePet(Pet pet){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.delete(pet);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
