package VetClinic.Dao;

import VetClinic.HibernateUtils;
import VetClinic.Model.Consult;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ConsultDao {
    public List<Consult> getAllConsults (){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Consult> consult = session.createQuery("from Consult", Consult.class).list();
            return consult;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void createConsult(Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(consult);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Consult findConsultById(long id){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Consult consult = session.find(Consult.class, id);
            return consult;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void updateConsult(Consult consult){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(consult);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void deleteConsult(Consult consult){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.delete(consult);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
