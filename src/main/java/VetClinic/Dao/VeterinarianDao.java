package VetClinic.Dao;

import VetClinic.HibernateUtils;
import VetClinic.Model.Veterinarian;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class VeterinarianDao {
    public List<Veterinarian> getAllVets (){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Veterinarian> pet = session.createQuery("from Veterinarian", Veterinarian.class).list();
            return pet;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void createVet(Veterinarian veterinarian) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(veterinarian);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Veterinarian findVetById(long id){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Veterinarian veterinarian = session.find(Veterinarian.class, id);
            return veterinarian;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void updateVet(Veterinarian veterinarian){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(veterinarian);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void deleteVet(Veterinarian veterinarian){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.delete(veterinarian);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
