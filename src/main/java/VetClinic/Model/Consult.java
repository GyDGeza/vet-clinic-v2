package VetClinic.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static VetClinic.HibernateUtils.dateFormat;

@Entity
@Table
public class Consult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "pet_id")
    private Pet pet;

    @ManyToOne
    @JoinColumn(name = "vet_id")
    private Veterinarian veterinarian;

//    @OneToMany(mappedBy = "consult")
//    private List<Veterinarian> veterinarianList;
//
//    @OneToMany(mappedBy = "consult")
//    private List<Pet> petList;

    @Column(name = "date")
    private GregorianCalendar date;

    @Column(name = "description")
    private String description;

    public Consult() {
    }

    public Consult(Veterinarian veterinarian, Pet pet, GregorianCalendar date, String description) {
        this.id = id;
        this.pet = pet;
        this.veterinarian = veterinarian;
        this.date = date;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Veterinarian getVeterinarian() {
        return veterinarian;
    }

    public void setVeterinarian(Veterinarian veterinarian) {
        this.veterinarian = veterinarian;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "\nConsult{" +
                "id=" + id +
                ", pet=" + pet +
                ", veterinarian=" + veterinarian +
                ", date=" + dateFormat(date) +
                ", description='" + description + '\'' +
                '}';
    }
}
