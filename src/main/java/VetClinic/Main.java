package VetClinic;

import VetClinic.Dao.PetDao;
import VetClinic.Menu.Menu;
import VetClinic.Model.Pet;
import org.hibernate.Session;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
//        PetDao petDao = new PetDao();
//        Pet pet = new Pet("Dog", new GregorianCalendar(120,12,3), true, "Greg");
//        petDao.createPet(pet);
        Session s = HibernateUtils.getSessionFactory().openSession();
        s.close();
        Menu menu = new Menu();
        int option = 1;
        while(option != 0) {
            menu.displayMenu();
            option = menu.chooseOption();
        }
    }


}
